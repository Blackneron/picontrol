<?php


// ----0--9--8--7--6--5--4--3--2--1--1--2--3--4--5--6--7--8--9--0---- //
// ================================================================== //
//                                                                    //
//                PiControl - Reboot / Shutdown Script                //
//                                                                    //
// ================================================================== //
//                                                                    //
//                      Version 1.0 / 09.04.2017                      //
//                                                                    //
//                      Copyright 2017 - PB-Soft                      //
//                                                                    //
//                           www.pb-soft.com                          //
//                                                                    //
//                           Patrick Biegel                           //
//                                                                    //
// ================================================================== //
//                                                                    //
//  If you want to get this to work on your computer, you have to     //
//  edit the 'sudoers file' and give the permission to execute the    //
//  'shutdown' command to the user under which runs the webserver -   //
//  in my case this user is called 'www-data'. With the command       //
//  'visudo' you can edit the 'sudoers file' and enter the following  //
//  line into the section which specifies the permissions:            //
//                                                                    //
//     www-data ALL = NOPASSWD: /sbin/shutdown                        //
//                                                                    //
//   The user 'www-data' now has the permission to execute a shut-    //
//   down or restart on your computer. Please be sure that you also   //
//   have set a long password which is not trivial to guess!          //
//                                                                    //
// ================================================================== //


// #####################################################################
// #####################################################################
// ##########      C O N F I G U R A T I O N - B E G I N      ##########
// #####################################################################
// #####################################################################


// =====================================================================
// Specify a plain password here to generate a hashed one. If you enter
// a plain text password here and execute the script, it will show you
// the hashed one and you can copy it to the variable '$password_hash'
// below. Then do not forget to delete the plain text password here!
// =====================================================================
$password_plain = "pass";


// =====================================================================
// Specify the hashed password (and please delete the plain text one!).
// =====================================================================
$password_hash = "";


// =====================================================================
// Specify the used password salt.
// =====================================================================
$password_salt = "j94j58guFF-erkid/ss";


// =====================================================================
// Specify if the IP address from the computer of the administrator has
// to be checked to reboot or shutdown this computer. If no IP address
// is specified, the IP address does not matter. The following types of
// IP address parts are possible:
//
//   192.168.0.45  ==>  Exact 1 IP address will match
//   192.168.0.    ==>  A small range of IP adresses will match
//   192.168.      ==>  A medium range of IP adresses will match
//   192.          ==>  A large range of IP adresses will match
//
// =====================================================================
$valid_ip_address = "192.168.0.";


// #####################################################################
// #####################################################################
// ##########        C O N F I G U R A T I O N - E N D        ##########
// #####################################################################
// #####################################################################


// =====================================================================
// Specify the company information.
// =====================================================================
$company = "PB-Soft";
$website = "pb-soft.com";


// =====================================================================
// Check if a plain text password was specified.
// =====================================================================
if ($password_plain != "") {


  // ===================================================================
  // Create and display a new password hash.
  // ===================================================================
  echo hash('sha256', $password_salt.$password_plain);


  // ===================================================================
  // Exit the script until only the hashed password is used.
  // ===================================================================
  exit;
}


// =====================================================================
// Display the HTML page - Begin.
// =====================================================================
echo "<!DOCTYPE html>\n";
echo "<html lang=\"en\">\n";
echo "<head>\n";
echo "<meta charset=\"utf-8\">\n";
echo "<title>Reboot ".$_SERVER["SERVER_NAME"]."</title>\n";


?>

    <style type="text/css">


      /* ===============================================================
      // All links.
      // =============================================================*/
      a {
        color: #3870A7;
        text-decoration: none;
      }


      /* ===============================================================
      // All links in hover mode.
      // =============================================================*/
      a:hover {
        color: #CC0033;
      }


      /* ===============================================================
      // Body.
      // =============================================================*/
      body {
        background-color: #dcebf7;
        color: #003366;
        font-family: Arial, Verdana, sans-serif;
        font-size: 20px;
        font-weight: bold;
      }


      /* ===============================================================
      // Main container.
      // =============================================================*/
      div.container {
        box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24), 0 17px 50px 0 rgba(0,0,0,0.19);
        background-color: #b9d3e8;
        border: 2px solid #4881ad;
        border-radius: 10px;
        margin: 80px auto;
        padding: 20px;
        text-align: center;
        width: 400px;
      }


      /* ===============================================================
      // Fieldset panel.
      // =============================================================*/
      fieldset.panel {
        border: 1px solid #4881ad;
        border-radius: 8px;
        padding-bottom: 40px;
      }


      /* ===============================================================
      // Important information.
      // =============================================================*/
      span.info {
        color: #FF0066;
      }


      /* ===============================================================
      // IP address title.
      // =============================================================*/
      h2.ip {
        font-size: 20px;
        padding-top: 20px;
      }


      /* ===============================================================
      // All input fields.
      // =============================================================*/
      input {
        font-family: Arial, Verdana, sans-serif;
        font-size: 14px;
        padding: 15px;
      }


      /* ===============================================================
      // Password input field.
      // =============================================================*/
      input[type="password"] {
        -moz-border-bottom-colors: none;
        -moz-border-left-colors: none;
        -moz-border-right-colors: none;
        -moz-border-top-colors: none;
        -moz-outline-radius: 3px;
        background: white none repeat scroll 0 0;
        border-image: none;
        border-radius: 2px;
        border-style: solid;
        border-width: 1px;
        color: #404040;
        height: 40px;
        margin: 20px;
        outline: 5px solid #eff4f7;
        padding: 0 10px;
        width: 220px;
        text-align: center;
      }


      /* ===============================================================
      // Action containers and submit container.
      // =============================================================*/
      div.reboot, div.shutdown, div.submit {
        padding: 10px;
      }


      /* ===============================================================
      // Submit button.
      // =============================================================*/
      input[type="submit"] {
        background-color: #008cba;
        border: medium none;
        border-radius: 8px;
        color: white;
        display: inline-block;
        font-size: 27px;
        padding: 8px 40px;
        text-align: center;
        text-decoration: none;
        transition-duration: 0.4s;
      }


      /* ===============================================================
      // Submit button - Hover mode.
      // =============================================================*/
      input[type="submit"]:hover {
        background-color: #4CAF50;
        color: white;
        background: #28597a;
        box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24), 0 17px 50px 0 rgba(0,0,0,0.19);
      }


      /* ===============================================================
      // Submit button - Active mode.
      // =============================================================*/
      input[type="submit"]:active {
        border-top-color: #1b435e;
        background: #1b435e;
      }


      /* ===============================================================
      // Result title of the action information.
      // =============================================================*/
      h2.result {
        font-size: 20px;
        padding: 0 0 5px;
      }


      /* ===============================================================
      // Hostname of the action information.
      // =============================================================*/
      div.result {
        color: #FF0066;
        padding: 10px 0 15px;
      }


      /* ===============================================================
      // Copyright message.
      // =============================================================*/
      div.copyright {
        background-color: #B9D3E8;
        color: #3870A7;
        font-size: 14px;
        margin-left: auto;
        margin-right: auto;
        margin-top: 20px;
        text-align: center;
      }


      /* ===============================================================
      // Copyright link.
      // =============================================================*/
      a.copyright {
        background-color: #B9D3E8;
        color: #0000FF;
        font-size: 14px;
      }

    </style>

<?php


// =====================================================================
// Display the HTML head - End.
// =====================================================================
echo "</head>\n";


// =====================================================================
// Display the HTML body - Begin.
// =====================================================================
echo "<body>\n";
echo "<div class=\"container\">\n";


// =====================================================================
// Check if the submit button was not pressed (show form).
// =====================================================================
if (!isset($_POST['submit'])) {


  // ===================================================================
  // Display the password / action form.
  // ===================================================================
  echo "<form id=\"reboot\" action=\"".$_SERVER['PHP_SELF']."\" method=\"post\" accept-charset=\"UTF-8\">\n";
  echo "<fieldset class=\"panel\" >\n";
  echo "<legend class=\"title\">Host: <span class=\"info\">".$_SERVER["SERVER_NAME"]."</span></legend>\n";
  echo "<h2 class=\"ip\">IP: <span class=\"info\">".$_SERVER["SERVER_ADDR"]."</span></h2>\n";
  echo "<div class=\"password\">\n";
  echo "<input class=\"password\" type=\"password\" name=\"password\" id=\"password\" />\n";
  echo "</div>\n";
  echo "<div class=\"reboot\">\n";
  echo "<input type=\"radio\" name=\"action\" value=\"reboot\" checked> Reboot\n";
  echo "</div>\n";
  echo "<div class=\"shutdown\">\n";
  echo "<input type=\"radio\" name=\"action\" value=\"shutdown\"> Shutdown\n";
  echo "</div>\n";
  echo "<div class=\"submit\">\n";
  echo "<input class=\"submit\" type=\"submit\" name=\"submit\" value=\"Go!\" />\n";
  echo "</div>\n";
  echo "</fieldset>\n";
  echo "</form>\n";


  // ===================================================================
  // The submit button was pressed (reboot or shutdown computer).
  // ===================================================================
} else {


  // ===================================================================
  // Search for the specified IP address in the actual IP address.
  // ===================================================================
  $position = strpos($_SERVER["REMOTE_ADDR"], $valid_ip_address);


  // ===================================================================
  // Check if the IP address is valid or not specified.
  // ===================================================================
  if ($position === 0 || $valid_ip_address == "") {


    // =================================================================
    // Check if the entered password is correct.
    // =================================================================
    if (hash('sha256', $password_salt.$_POST['password']) == $password_hash) {


      // ===============================================================
      // Check if the reboot action was selected.
      // ===============================================================
      if (isset($_POST['action']) && $_POST['action'] == "reboot") {


        // =============================================================
        // Display an information message for the user - Reboot.
        // =============================================================
        echo "<h2 class=\"result\">Reboot host:</h2>";
        echo "<div class=\"result\">".$_SERVER["SERVER_NAME"]."</div>";


        // =============================================================
        // Reboot the computer.
        // =============================================================
        system('sudo /sbin/shutdown -r now');


        // =============================================================
        // Check if the shutdown action was selected.
        // =============================================================
      } elseif (isset($_POST['action']) && $_POST['action'] == "shutdown") {


        // =============================================================
        // Display an information message for the user - Shutdown.
        // =============================================================
        echo "<h2 class=\"result\">Shutdown host:</h2>";
        echo "<div class=\"result\">".$_SERVER["SERVER_NAME"]."</div>";


        // =============================================================
        // Shutdown the computer.
        // =============================================================
        system('sudo /sbin/shutdown -h now');
      }


      // ===============================================================
      // The entered password is not valid.
      // ===============================================================
    } else {


      // ===============================================================
      // Display an information message for the user - No error message!
      // ===============================================================
      echo "<h2 class=\"result\">Great, now have a nice day!</h2>";
    }
  }
}


// =====================================================================
// Display the copyright.
// =====================================================================
echo"<div class=\"copyright\">\n";
echo"Copyright &copy; ".date('Y')." - <a class=\"copyright\" href=\"http://".$website."\" onclick=\"window.open(this.href, 'PB-Soft').focus(); return false;\">".$company."</a>\n";
echo"</div>\n";


// =====================================================================
// Display the HTML page - End.
// =====================================================================
echo "</div>\n";
echo "</body>\n";
echo "</html>\n";
