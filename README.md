# PiControl Tool - README #
---

### Overview ###

The **PiControl** tool (PHP) can be installed on a Raspberry Pi and you can reboot or shutdown the computer via the small panel (webpage). It is necessary to enter a password to use the tool and optionally the IP address of the administrators PC has to match. Everything is in one file and in the configuration section you can specify the password and allowed IP address (or a range).

### Screenshots ###

![PiControl Screen 1](development/readme/picontrol1.png "PiControl Screen 1")

### Setup ###

* Copy the folder 'picontrol' to your webroot directory.
* Open the file 'picontrol.php' in an editor and enter a plaintext password.
* Access the PiControl tool from your computer:
  ==>  https://your.domain.com/picontrol/picontrol.php
* The script will generate a hashed value from your password and display it.
* Enter the hashed password into the configuration section of the script.
* Delete the plaintext password from the configuration section!
* Enter an IP address from which you will access the PiControl tool (optional).
* Edit the 'sudoer file' on your Raspberry Pi with the command 'visudo'.
* Enter the following line into the section which specifies the permissions:
* ==>  www-data ALL = NOPASSWD: /sbin/shutdown
* Save the file and exit.

### Support ###

This is a free tool and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### License ###

The **PiControl** tool is licensed under the [**MIT License (Expat)**](https://pb-soft.com/resources/mit_license/license.html) which is published on the official site of the [**Open Source Initiative**](https://opensource.org/licenses/MIT).
